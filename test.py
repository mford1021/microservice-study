def only_the_red(cars):
    results = []

    for car in cars:
        if int(car.get("year")) < 1980 and int(car.get("mpg")) < 12:
            results.append(car)
    return results

cars = [
    {"type": "truck", "color": "rusted", "year": 1955},
    {"type": "sedan", "color": "red", "year": 2015},
    {"type": "wagon", "color": "pea green", "year": 1965},
    {"type": "truck", "color": "blue", "year": 1995},
]

print(only_the_red(cars))