def fizzbuzz(maximum):
    output = []
    for num in range(1,maximum+1):
        if (num%3) == 0 and (num%5) == 0:
            output.append("fizzbuzz")
        elif (num%3) == 0:
            output.append("fizz")
        elif (num%5) == 0:
            output.append("buzz")
        else:
            output.append(str(num))
    return output

print(fizzbuzz(6))