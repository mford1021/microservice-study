def old_guzzlers(cars):
    results = []
    
    for car in cars:
        length = car.get("length")
        weight = car.get("max_weight")
        if isinstance(length, int) and isinstance(weight,int):
            if length > 25 or weight > 4000:
                results.append(car)
    return results

cars = [
    {"type": "truck", "color": "rusted", "year": 1955},
    {"type": "sedan", "color": "red", "year": 2015},
    {"type": "wagon", "color": "pea green", "year": 1965},
    {"type": "truck", "color": "blue", "year": 1995},
    {'make': 'ford', 'mpg': 8, 'year': 1955},
    {'make': 'ford', 'mpg': 11, 'year': 1978}
]

print(old_guzzlers(cars))